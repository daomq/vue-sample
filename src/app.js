import { createSSRApp } from 'vue';
import { createRouter } from './router';
import { createStore } from './store';
import { createHead } from '@vueuse/head';
// import { sync } from 'vuex-router-sync';
import { sync } from 'vuex-router-sync/dist/vuex-router-sync.cjs.js'

import App from './App.vue'

// import mitt from 'mitt';
// const emitter = mitt();

export default function buildApp() {
  const app = createSSRApp(App);

  const router = createRouter();
  const store = createStore();
  const head = createHead();

  sync(store, router);

  app.use(router);
  app.use(store);
  app.use(head);
  // app.provide('emitter', emitter);

  return { app, router, store, head };
}
