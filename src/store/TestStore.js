import TestApi from '@/api/TestApi'

const TestStore = {
  namespaced: true,
  state() {
    return {
      tripData: null
    };
  },
  actions: {
    async getTripInfo ({commit}, params) {
      await TestApi.get(params).then(
        (resp) => {
          console.log('actions getTripInfo completed')
          commit('setTripData', resp)
        },
        (err) => console.log(err.message)
      )
    }
  },
  mutations: {
    setTripData (state, response) {
      console.log('mutations setTripData')
      state.tripData = response.data
    }
  },
  getters: {
    getTripData (state) {
      // console.log('getters setTripData', state.tripData)
      return state.tripData
    }
  }
}

export default TestStore
