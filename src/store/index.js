import {
  createStore as _createStore,
  createLogger as _createLogger
} from 'vuex'

import TestStore from './TestStore'

const isProd = process.env.NODE_ENV !== 'production'

export function createStore() {
  return _createStore({
    modules: {
      TestStore
    },
    strict: isProd,
    plugins: isProd ? [] : [_createLogger()]
  })
}
