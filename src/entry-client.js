import buildApp from './app'

const { app, router, store } = buildApp()

// eslint-disable-next-line no-underscore-dangle
if (window.__INITIAL_DATA__) {
  let storeInitialState = {
    ...window.__INITIAL_DATA__
  }

  if (storeInitialState) {
    store.replaceState(storeInitialState);
  }
}

router.isReady()
  .then(() => {
    app.mount('#app', true);
  })
