import { createMemoryHistory, createWebHistory, createRouter as _createRouter } from 'vue-router'
import routes from "./routes"

const isServer = typeof window === 'undefined'
const history = isServer ? createMemoryHistory() : createWebHistory()

export function createRouter() {
  return _createRouter({
    history,
    routes,
    scrollBehavior: (to) => {
      if (to.hash) {
          return {
              el: to.hash,
              behavior: 'smooth'
          }
      } else {
          return { top: 0 }
      }
    }
  })
}
