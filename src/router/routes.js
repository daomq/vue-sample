const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('../pages/Home')
  },
  {
    path: '/trip-gallery',
    name: 'trip-gallery',
    component: () => import('../pages/TripGallery')
  },
  {
    path: '/ui',
    name: 'ui-elements',
    component: () => import('../pages/UIElements')
  },
  {
    path: '/:pathMatch(.*)*',
    name: '404',
    component: () => import('../pages/Fake')
  }
]

export default routes
