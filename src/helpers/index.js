export function useSSR() {
  return typeof window === 'undefined'
}

export function isProd() {
  return process.env.NODE_ENV === 'production'
}

export function scrollBehavior(to) {
  if (to.hash) {
    return {
      el: to.hash,
      behavior: 'smooth'
    }
  }

  return {top: 0}
}
