export const axios = require('axios')
export const isBrowser = typeof window !== 'undefined'

export const ua = () => {
    const userAgentInfo = isBrowser ? navigator.userAgent : ''
    const Agents = ['Android', 'iPhone', 'SymbianOS', 'Windows Phone', 'iPod']
    let flag = 'PC'
    for (let vv = 0; vv < Agents.length; vv++) {
        if (userAgentInfo.indexOf(Agents[vv]) > 0) {
            flag = Agents[vv]
            break
        }
    }
    return flag
}

export const sleep = ms => {
    return new Promise(resolve => setTimeout(resolve, ms))
}

export const createHttp = url => {
    const http = axios.create({
        baseURL: url || null
    })
    return http
}

export const ps = (moduleName) => { // Preserve Vuex State
    if (isBrowser) {
        if (window.__INITIAL_DATA__ !== undefined && window.__INITIAL_DATA__[moduleName] !== undefined) {
            delete window.__INITIAL_DATA__[moduleName]
            return true
        }
    }
    return false
}