import {TripEndpoint} from './TripEndpoint'
import {createHttp} from '@/utils'

const baseUrl = TripEndpoint;

let client = createHttp(baseUrl);

export default {
    get(params) {
        return client.get('/gettripdetails/' + params.id, { params: { tcid: params.data.tcid, key: params.data.key, type: params.data.type } })
    },
    getTripList(params) {
        return client.get('/gettriplist/', { params: { tcid: params.data.tcid, key: params.data.key, type: params.data.type } })
    },
    getTest() {
        return client.get('https://jsonplaceholder.typicode.com/users')
    }
}
