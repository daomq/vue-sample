# designerjourneys

## Project setup
Create .env file and run the command below
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Style Guide

Follow the instruction [here](https://docs.google.com/document/d/1MdLqYPDbNZ96qu8EjNx7RjrHfaBY9umsDMWKfPTokb4/edit)
