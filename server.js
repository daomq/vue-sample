/* eslint-disable no-param-reassign */
/* eslint-disable import/no-dynamic-require */
require('dotenv').config()
const path = require('path')
const childProcess = require('child_process')
const express = require('express')
const fs = require('fs')
const { renderToString } = require('@vue/server-renderer')
const { renderHeadToString } = require('@vueuse/head')
const manifest = require('./dist/server/ssr-manifest.json')

// Create the express app.
const server = express()
const serialize = require('serialize-javascript')

// we do not know the name of app.js as when its built it has a hash name
// the manifest file contains the mapping of "app.js" to the hash file which was created
// therefore get the value from the manifest file thats located in the "dist" directory
// and use it to get the Vue App
const appPath = path.join(__dirname, './dist', 'server', manifest['app.js'].replace('http://travel-explorer.local/', ''))
const createApp = require(appPath).default

const clientDistPath = './dist/client'
server.use('/img', express.static(path.join(__dirname, clientDistPath, 'img')))
server.use('/js', express.static(path.join(__dirname, clientDistPath, 'js')))
server.use('/css', express.static(path.join(__dirname, clientDistPath, 'css')))
server.use('/favicon.ico', express.static(path.join(__dirname, clientDistPath, 'favicon.ico')))

//let logFile = fs.createWriteStream('./myLogFile.log', {flags: 'a'});
//server.use(express.logger({stream: logFile}));

// handle all routes in our application
server.get('*', async (req, res) => {

  const { app, store, head } = await createApp(req.url)

  let appContent = await renderToString(app)

  const { headTags } = await renderHeadToString(head)
  const renderState = `
    <script>
      window.__INITIAL_DATA__ = ${serialize(store.state)}
    </script>`

  fs.readFile(path.join(__dirname, clientDistPath, 'index.html'), (err, html) => {
    if (err) {
      throw err
    }

    appContent = `<div id="app">${appContent}</div>`

    html = html.toString()
                .replace('<div id="app"></div>', `${renderState}${appContent}`)
                .replace('<head>', `<head>${headTags}`)
    res.setHeader('Content-Type', 'text/html')
    res.send(html)
  })
})

const port = process.env.PORT || 8080
const host = process.env.HOST || 'http://localhost'
server.listen(port, () => {
  let url = `${host}:${port}/`
  console.log(`You can navigate to ${url}`)
  // childProcess.exec('start ' + url);
})
