// Require the framework and instantiate it
require('dotenv').config()
const path = require('path')
const fs = require('fs')
const { renderToString } = require('@vue/server-renderer')
const { renderHeadToString } = require('@vueuse/head')
const manifest = require('./dist/server/ssr-manifest.json')
const serialize = require('serialize-javascript')
const Redis = require('ioredis')
const {default: RedisStore} = require('@mgcrea/fastify-session-redis-store')
// init vue app
const appPath = path.join(__dirname, './dist', 'server', manifest['app.js'])
const createApp = require(appPath).default
const clientDistPath = './dist/client'

//init web server
const fastifySession = require('@mgcrea/fastify-session');
const fastifyCookie = require('fastify-cookie');

const fastify = require('fastify')({ 
    logger: false,
    trustProxy: true
})
const fStatic = require('fastify-static')
const statics = {
    img: path.join(__dirname, clientDistPath, 'img'),
    js: path.join(__dirname, clientDistPath, 'js'),
    css: path.join(__dirname, clientDistPath, 'css')
}
let decorate=true
for (const prefix in statics) {
    if (Object.hasOwnProperty.call(statics, prefix)) {
        const root = statics[prefix];
        fastify.register(fStatic, {
            root: root,
            prefix: `/${prefix}`,
            decorateReply: decorate
        })
        decorate = false
    }
}
fastify.get('/favicon.ico', async (request, reply) => {
    return reply.header('Content-Type', 'image/x-icon')
        .send(fs.createReadStream(path.join(__dirname, clientDistPath,'favicon.ico')))
})

// define session
fastify.register(fastifyCookie);
fastify.register(fastifySession, {
    store: new RedisStore({ client: new Redis({
        host: process.env.REDIS_HOST,
        port: process.env.REDIS_PORT,
        db: process.env.REDIS_DB
    }), ttl:  process.env.SESSION_TTL, prefix: 'dj_sess' }),
    secret: 'JMR4axnQLswYTmJw94R75PEXH3sDxM4PY8eUXfMj3BSsVEht',
    cookieName: 'djSessId',
    cookie:{
        path:'/',
        httpOnly: true,
        secure: false,
        sameSite: 'lax'
    },
    
});
// Declare a route
fastify.get('*', async (request, reply) => {
    // console.log(request.session.get('test'))
    // request.session.set('test', 'data')
    const { app, store, head } = await createApp(request.url)
    let appContent = await renderToString(app)
    const { headTags } = renderHeadToString(head)

    const renderState = `
    <script>
      window.__INITIAL_DATA__ = ${serialize(store.state)}
    </script>`
    
    try{
        let html = await fs.readFileSync(path.join(__dirname, clientDistPath, 'index.html'))

        appContent = `<div id="app">${appContent}</div>`
        html = html.toString()

        html = html.replace('<div id="app"></div>', `${renderState}${appContent}`)
                .replace('<head>', `<head>${headTags}`)
        
        reply.header('Content-Type', 'text/html; charset=utf-8')

        reply.send(html);
    }catch(err){
        console.error(err)
        reply.send(new Error("Internal Server Error"))
    }    
})

const port = process.env.PORT || 8080
const host = process.env.HOST || '0.0.0.0'

// Run the server!
const start = async () => {
    try {
      await fastify.listen(port, host)
      console.log(`You can navigate to http://${host}:${port}`)
    } catch (err) {
      fastify.log.error(err)
      process.exit(1)
    }
}
start()